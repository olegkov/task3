package com.mmtr.spring.task3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppThird {
    public static void main(String[] args) {
        SpringApplication.run(AppThird.class, args);
    }
}
