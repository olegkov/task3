package com.mmtr.spring.task3.controller;

import com.mmtr.spring.task3.model.Dictionary1;
import com.mmtr.spring.task3.model.Dictionary2;
import com.mmtr.spring.task3.service.ServiceCRUD;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@Controller
public class MyController {
    private String dictionaryName = "Dictionary1";

    @Autowired
    private ServiceCRUD serviceCRUD;

    @GetMapping("/")
    public String getHome(){
        return "main";
    }

    @PostMapping("/")
    public String postHome(@RequestParam String dictionaryName){
        this.dictionaryName = dictionaryName;
        return "menu";
    }

    @GetMapping("/menu")
    public String getMenu(){
        return "menu";
    }

    @GetMapping("/showAll")
    public String getShowAll(Model model){
        switch(dictionaryName){
            case "Dictionary1":
                model.addAttribute("records", serviceCRUD.findAllFirst());
                break;

            case "Dictionary2":
                model.addAttribute("records", serviceCRUD.findAllSecond());
                break;
        }

        return "showAll";
    }

    @GetMapping("/search")
    public String getSearch(){
        switch (dictionaryName){
            case "Dictionary1":
                return "redirect:/searchFirst";
            case "Dictionary2":
                return "redirect:/searchSecond";
        }
        return "menu";
    }

    @GetMapping("/add")
    public String getAdd(){
        switch (dictionaryName){
            case "Dictionary1":
                return "redirect:/addFirst";
            case "Dictionary2":
                return "redirect:/addSecond";
        }
        return "menu";
    }

    @GetMapping("/delete")
    public String getDelete(){
        switch (dictionaryName){
            case "Dictionary1":
                System.out.println("Dictionary1");
                return "redirect:/deleteFirst";
            case "Dictionary2":
                return "redirect:/deleteSecond";
        }
        return "menu";
    }

    @GetMapping("/addFirst")
    public String getAdd(Dictionary1 dictionary){
        return "addFirst";
    }

    @PostMapping("/addFirst")
    public String postAdd(@Valid Dictionary1 dictionary, BindingResult result){
        if(result.hasErrors()){
            return "addFirst";
        }

        serviceCRUD.save(dictionary);
        return "menu";
    }

    @GetMapping("/addSecond")
    public String getAdd(Dictionary2 dictionary){
        return "addSecond";
    }

    @PostMapping("/addSecond")
    public String postAdd(@Valid Dictionary2 dictionary, BindingResult result){
        if(result.hasErrors()){
            return "addSecond";
        }
        serviceCRUD.save(dictionary);
        return "menu";
    }

    @GetMapping("/deleteFirst")
    public String getDelete(Dictionary1 dictionary){
        return "deleteFirst";
    }

    @PostMapping("/deleteFirst")
    public String postDelete(@ModelAttribute Dictionary1 dict){
        List<Dictionary1> dictionaries = serviceCRUD.findByKeyFirst(dict.getKey());
        if(dictionaries == null){
            return "menu";
        }

        for(Dictionary1 dictionary : dictionaries){
            serviceCRUD.delete(dictionary);
        }
        return "menu";
    }

    @GetMapping("/deleteSecond")
    public String getDelete(Dictionary2 dictionary){
        return "deleteSecond";
    }

    @PostMapping("/deleteSecond")
    public String postDelete(@ModelAttribute Dictionary2 dict){
        List<Dictionary2> dictionaries = serviceCRUD.findByKeySecond(dict.getKey());
        if(dictionaries == null){
            return "menu";
        }

        for(Dictionary2 dictionary : dictionaries){
            serviceCRUD.delete(dictionary);
        }
        return "menu";
    }

    @GetMapping("/searchFirst")
    public String getSearch(Dictionary1 dictionary){
        return "searchFirst";
    }

    @GetMapping("/searchSecond")
    public String getSearch(Dictionary2 dictionary){
        return "searchSecond";
    }

    @PostMapping("/searchFirst")
    public String postSearch(@ModelAttribute Dictionary1 dict, Model model){
        List<Dictionary1> dictionaries = serviceCRUD.findByKeyFirst(dict.getKey());
        if(dictionaries.isEmpty()){
            return "menu";
        }

        model.addAttribute("dictionaries", dictionaries);
        return "showRecordFirst";
    }

    @PostMapping("/searchSecond")
    public String postSearch(@ModelAttribute Dictionary2 dict, Model model){
        List<Dictionary2> dictionaries = serviceCRUD.findByKeySecond(dict.getKey());
        if(dictionaries.isEmpty()){
            return "menu";
        }

        model.addAttribute("dictionaries", dictionaries);
        return "showRecordSecond";
    }
}
