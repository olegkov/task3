package com.mmtr.spring.task3.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "dictionary2")
public class Dictionary2{
    @Id
    @Column(name = "id")
//    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Pattern(regexp = "^(?<key>\\d{5})$")
    @Column(name = "key_")
    private String key;
    @Column(name = "value")
    private String value;

    public Dictionary2(){}

    public Dictionary2(Integer id, String key, String value) {
        this.id = id;
        this.key = key;
        this.value = value;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
