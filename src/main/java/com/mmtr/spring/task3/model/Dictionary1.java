package com.mmtr.spring.task3.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "dictionary1")
public class Dictionary1{
    @Id
    @Column(name = "id")
//    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Pattern(regexp = "^(?<key>[a-zA-z]{4})$")
    @Column(name = "key_")
    private String key;
    @Column(name = "value")
    private String value;

    public Dictionary1(){}

    public Dictionary1(Integer id, String key, String value) {
        this.id = id;
        this.key = key;
        this.value = value;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
