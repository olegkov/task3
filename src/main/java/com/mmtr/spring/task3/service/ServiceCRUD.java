package com.mmtr.spring.task3.service;

import com.mmtr.spring.task3.model.Dictionary1;
import com.mmtr.spring.task3.model.Dictionary2;
import com.mmtr.spring.task3.repository.RepositoryCRUD1;
import com.mmtr.spring.task3.repository.RepositoryCRUD2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceCRUD {
    @Autowired
    private RepositoryCRUD1 repository1;
    @Autowired
    private RepositoryCRUD2 repository2;

    public ServiceCRUD(RepositoryCRUD1 repository1, RepositoryCRUD2 repository2){
        this.repository1 = repository1;
        this.repository2 = repository2;
    }

    public Iterable<Dictionary1> findAllFirst(){
        return repository1.findAll();
    }

    public void save(Dictionary1 dictionary){
        repository1.save(dictionary);
    }

    public void delete(Dictionary1 dictionary){
        repository1.delete(dictionary);
    }

    public List<Dictionary1> findByKeyFirst(String key){
        return repository1.findByKey(key);
    }

    public Iterable<Dictionary2> findAllSecond(){
        return repository2.findAll();
    }

    public void save(Dictionary2 dictionary){
        repository2.save(dictionary);
    }

    public void delete(Dictionary2 dictionary){
        repository2.delete(dictionary);
    }

    public List<Dictionary2> findByKeySecond(String key){
        return repository2.findByKey(key);
    }
}
