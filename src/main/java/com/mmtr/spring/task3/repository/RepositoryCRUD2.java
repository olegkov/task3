package com.mmtr.spring.task3.repository;

import com.mmtr.spring.task3.model.Dictionary2;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositoryCRUD2 extends CrudRepository<Dictionary2, Integer> {
    @Query("SELECT a FROM Dictionary2 a WHERE a.key = :key")
    List<Dictionary2> findByKey(@Param("key") String key);

    @Query("SELECT new Dictionary2(id, key, value) from Dictionary2")
    List<Dictionary2> findAll();
}
