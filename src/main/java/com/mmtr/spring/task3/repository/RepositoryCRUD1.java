package com.mmtr.spring.task3.repository;

import com.mmtr.spring.task3.model.Dictionary1;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RepositoryCRUD1 extends CrudRepository<Dictionary1, Integer> {
    @Query("SELECT a FROM Dictionary1 a WHERE a.key = :key")
    List<Dictionary1> findByKey(@Param("key") String key);

    @Query("SELECT new Dictionary1(id, key, value) from Dictionary1")
    List<Dictionary1> findAll();
}
