DROP TABLE IF EXISTS dictionary1;
DROP TABLE IF EXISTS dictionary2;
DROP TABLE IF EXISTS BILLIONAIRES;

CREATE TABLE dictionary1(
    id INT AUTO_INCREMENT  PRIMARY KEY,
    key_ VARCHAR(4) NOT NULL,
    value VARCHAR(20) NOT NULL
);

INSERT INTO dictionary1 (key_, value) VALUES ('here', 'здесь');
INSERT INTO dictionary1 (key_, value) VALUES ('test', 'тест');
INSERT INTO dictionary1 (key_, value) VALUES ('fire', 'огонь');
INSERT INTO dictionary1 (key_, value) VALUES ('hell', 'хелл');
INSERT INTO dictionary1 (key_, value) VALUES ('home', 'дом');

CREATE TABLE dictionary2(
    id INT AUTO_INCREMENT  PRIMARY KEY,
    key_ VARCHAR(5) NOT NULL,
    value VARCHAR(20) NOT NULL
);

INSERT INTO dictionary2 (key_, value) VALUES ('34567', 'fire');
INSERT INTO dictionary2 (key_, value) VALUES ('45678', 'value');
INSERT INTO dictionary2 (key_, value) VALUES ('56789', 'home');
INSERT INTO dictionary2 (key_, value) VALUES ('11111', '11111');
INSERT INTO dictionary2 (key_, value) VALUES ('12345', 'here');
INSERT INTO dictionary2 (key_, value) VALUES ('23456', 'door');